package com.shb.service;

import com.shb.model.Sgat9460K001;
import com.shb.model.Sgat9461K001;
import com.shb.util.XmlLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class AnyLinkService {

  private static final Logger logger = LoggerFactory.getLogger(AnyLinkService.class);

  @Autowired
  XmlLoader xmlLoader;

  @SuppressWarnings("unused")
  public Sgat9461K001 process(Sgat9460K001 sgat9460K001) throws Exception {

    Sgat9461K001 result = xmlLoader.load(
        "template/SGAT9461K001.xml", Sgat9461K001.class);
    return result;
  }
}
