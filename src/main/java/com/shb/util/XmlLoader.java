package com.shb.util;

import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.freemarker.FreemarkerConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class XmlLoader {

  final
  ProducerTemplate producer;

  @Autowired
  public XmlLoader(ProducerTemplate producerTemplate) {
    this.producer = producerTemplate;
  }

  public <T> T load(String template, Class<T> type) throws Exception {

    T result = producer.requestBodyAndHeader(
        "direct:loadXml",
        "",
        FreemarkerConstants.FREEMARKER_RESOURCE_URI,
        template,
        type);

    return result;
  }
}
